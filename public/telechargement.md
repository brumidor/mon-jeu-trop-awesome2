# T&eacute;l&eacute;chargement

## Code Source

- [Format zip](https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome2/-/archive/main/mon-jeu-trop-awesome2-main.zip)
- [Format tar.gz](https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome2/-/archive/main/mon-jeu-trop-awesome2-main.tar.gz)
- [Format tar.bz2](https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome2/-/archive/main/mon-jeu-trop-awesome2-main.tar.bz2)
- [Format tar](https://gitlab.com/exemples-420-2n3-dm/mon-jeu-trop-awesome2/-/archive/main/mon-jeu-trop-awesome2-main.tar)
